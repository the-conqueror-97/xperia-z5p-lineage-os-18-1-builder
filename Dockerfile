FROM ubuntu:18.04


ENV GIT_MAIL "fatih.gucuko@protonmail.com"
ENV GIT_NAME "Fatih Gucuko"

ENV BRANCH "lineage-18.1"
ENV MANIFEST_BRANCH "lineage-18.1"


RUN dpkg --add-architecture i386
RUN apt-get update -y && apt-get upgrade -y

RUN apt-get install repo python3 aptitude software-properties-common bc bison build-essential ccache wget curl flex g++-multilib gcc-multilib git gnupg gperf imagemagick lib32ncurses5-dev lib32readline-dev lib32z1-dev liblz4-tool libncurses5 libncurses5-dev libsdl1.2-dev libssl-dev libxml2 libxml2-utils lzop pngcrush rsync schedtool squashfs-tools xsltproc zip zlib1g-dev -y
# RUN ln -s /usr/bin/python3 /usr/bin/python

# add open-dk ppa
RUN add-apt-repository ppa:openjdk-r/ppa
RUN apt-get update -y && apt-get upgrade -y

# Add to bin
RUN mkdir ~/bin
RUN curl http://commondatastorage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
RUN chmod a+x ~/bin/repo

RUN export PATH=~/bin:$PATH

# Configure git
RUN git config --global user.email "$GIT_MAIL"
RUN git config --global user.name "$GIT_NAME"

RUN mkdir ~/android
RUN cd ~/android && repo init -u https://github.com/LineageOS/android.git -b $BRANCH 


COPY src/lineage-18.1-manifests.xml ~/android/.repo/local_manifests/manifests.xml
